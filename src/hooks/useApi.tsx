import React from 'react';
import Req from '../util/AxiosRequest';
import { UNPROCESSABLE } from '../settings/Errors';

const data: any = new Req();

interface StateType {
	isLoading: boolean;
	hasData: boolean;
	data: object[];
	errors: object[];
}

interface ActionType {
	type: 'INIT' | 'SUCCESS' | 'FAILURE' | 'CLEAR';
	payload?: [{}];
}

const reducer = (state: StateType, action: any) => {
	switch (action.type) {
		case 'INIT':
			return {
				...state,
				status: null,
				isLoading: true,
				hasData: false,
				errors: [],
			};
		case 'SUCCESS':
			return {
				...state,
				isLoading: false,
				status: 200,
				hasData: Array.isArray(action.payload) ? !!action.payload.length : action.payload,
				data: action.payload,
			};
		case 'FAILURE':
			return {
				...state,
				isLoading: false,
				status: action.response.status,
			};
		case 'CATCH_ERRORS':
			return {
				...state,
				errors: action.response.errors,
				status: action.response.status,
				isLoading: false,
			};
		case 'CLEAR':
			return {
				...state,
				errors: action.payload,
			};
		default:
			throw new Error();
	}
};

interface Url {
	method: string;
	endpoint: (payload?: any) => object | string;
}

function useApi(url: Url) {
	const [apiState, dispatch] = React.useReducer(reducer, {
		status: null,
		isLoading: false,
		hasData: false,
		data: [],
		errors: [],
	});

	const api: any = async (payload?: any, query?: string) => {
		dispatch({ type: 'INIT' });
		const { method, endpoint } = url;
		try {
			const response = await data[method](endpoint(query), payload);
			dispatch({ type: 'SUCCESS', payload: response.data });
			return response;
		} catch (errors) {
			const response = {
				status: errors.status,
				errors: UNPROCESSABLE.includes(errors.status) ? errors.data : [],
			};
			if (!UNPROCESSABLE.includes(errors.status)) dispatch({ type: 'FAILURE', response });
			else if (UNPROCESSABLE.includes(errors.status)) dispatch({ type: 'CATCH_ERRORS', response });
			return errors;
		}
	};

	const hasError: any = (field: string) => {
		const { errors } = apiState;
		return errors.some((error: any) => error.param === field);
	};

	const hasErrors: any = () => {
		const { errors } = apiState;
		return !!errors.length;
	};

	const getErrors: any = (field: string) => {
		const { errors } = apiState;
		const newErrors = errors.filter((error: any) => error.param === field);
		if (newErrors.length) return newErrors[0].errors;
		return [];
	};

	const clearError: any = (field?: string) => {
		if (field) {
			const { errors } = apiState;
			dispatch({ type: 'CLEAR', payload: errors.filter((error: any) => error.param !== field) });
		} else if (!field) {
			dispatch({ type: 'CLEAR', payload: [] });
		}
	};

	const errorsHelpers = {
		hasError,
		hasErrors,
		getErrors,
		clearError,
	};

	return [api, apiState, errorsHelpers];
}

export default useApi;
